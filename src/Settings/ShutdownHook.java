/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Settings;

import Evaluator.SnifferApp;

/**
 *
 * @author LightMind
 */
public class ShutdownHook extends Thread{
    public void run() {
        System.out.println("Jumlah paket yang diambil: "+SnifferApp.JmlPaket);
        System.out.println("Delay Minimum: "+SnifferApp.MinDelay);
        System.out.println("Delay Maksimum: "+SnifferApp.MaxDelay);
        System.out.println("Delay Rata-rata: "+(SnifferApp.JmlDelay / SnifferApp.JmlPaket));
        System.out.println("Standar Deviasi Delay: "+Math.sqrt(((SnifferApp.JmlPaket * SnifferApp.JmlDelayKuadrat) - Math.pow(SnifferApp.JmlDelay, 2)) / (SnifferApp.JmlPaket * (SnifferApp.JmlPaket-1))));    
    }
}
