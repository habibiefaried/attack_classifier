/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Settings;

/**
 *
 * @author LightMind
 */
public class GlobalSettings {
    public static boolean useRealDataset = true;
    
    /**  **/
    public static boolean terbalik = false;
    public static String dataset = "C:\\logTA\\terbalik\\week1-tuesday-NN.dataset";
    public static String datatest = "C:\\logTA\\terbalik\\dataset-full.datatest";
    
   /** Khusus untuk datatest yang menggunakan tcpdump **/
    public static boolean modedump = false;
    public static String tcpdatatest = "C:\\logTA\\test.pcap";
    
    public static int MaximumNeighbour = 50;
    public static String week = "week5";
    public static String day = "thursday";
    public static String active_dir = "D:\\OprekanTA\\dataset\\"+week+"\\"+day+"\\";
    public static String learn_dir = "C:\\logTA\\";
    public static String work_file = "C:\\logTA\\testing\\week5-thursday-NN.datatest";
    public static String hasil_file = work_file+".hasilNN";   
    
    public static String normalizeIP(String IP){
        String balikan = "";
        String[] parts = IP.split("\\.");
        int idx = 0;
        for (String p : parts) {
            //System.out.println(p);
            idx++;
            Integer T = Integer.parseInt(p);
            balikan = balikan + T.toString();
            if (idx != 4)
            balikan = balikan + ".";
            
            
        }
        return balikan;
    }
}
