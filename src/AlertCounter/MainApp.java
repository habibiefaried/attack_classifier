/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlertCounter;

import Settings.GlobalSettings;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 *
 * @author Kucing
 */
public class MainApp {
    private static HashMap<String, ArrayList<SnortAlert>> AnswerList = new HashMap<>();
    public static double jmlPositif = 0;
    public static double jmlNegatif = 0;
    public static void readAnswer(){
        try {
            File file = new File("C:\\logTA\\dataSNORT\\tcpdump.list");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                ArrayList<SnortAlert> temp = new ArrayList<SnortAlert>();
                //System.out.println(line);
                StringTokenizer st = new StringTokenizer(line," "); 
                SnortAlert AA = new SnortAlert();
                st.nextToken(); st.nextToken();
                AA.waktu = st.nextToken();
                st.nextToken(); st.nextToken();
                AA.port_src = st.nextToken();
                AA.port_dst = st.nextToken();
                AA.ip_src = GlobalSettings.normalizeIP(st.nextToken());
                AA.ip_dst = GlobalSettings.normalizeIP(st.nextToken());
                st.nextToken();
                AA.verdict = st.nextToken();
                if (AA.verdict.equals("-")) jmlNegatif++;
                else jmlPositif++;
                
                if (!AnswerList.containsKey(AA.waktu)) {
                    temp.add(AA);
                    AnswerList.put(AA.waktu, temp);
                } else {
                    temp = AnswerList.get(AA.waktu);
                    //System.out.println(temp);
                    temp.add(AA);
                    AnswerList.put(AA.waktu, temp);
                }
            }
            fileReader.close();
            //System.out.println(AnswerList.get("19:18:38"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static String getWaktu(String line){
        String hasil = "";
        StringTokenizer st = new StringTokenizer(line,".");
        StringTokenizer st2 = new StringTokenizer(st.nextToken(),"-");
        st2.nextToken();
        hasil = st2.nextToken();
        return hasil;
    }
    
    public static void main(String[] args) {
        readAnswer();
        PrintWriter out;
        String idxx = "C:\\logTA\\dataSNORT\\alert-week6-selasa";
        double countTrue = 0;
        double countFalse = 0;
        try {
            out = new PrintWriter(new FileWriter(idxx+"-hasil"));
            File file = new File(idxx);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {     
                if (line.length() > 0) {
                    if (line.charAt(0) == '0') {
                        StringTokenizer st = new StringTokenizer(line," ");
                        SnortAlert AA = new SnortAlert();
                        AA.waktu = getWaktu(st.nextToken());
                        String temp_src = st.nextToken();
                        StringTokenizer st2 = new StringTokenizer(temp_src,":");
                        AA.ip_src = st2.nextToken();
                        if (st2.hasMoreTokens()) AA.port_src = st2.nextToken();
                        else AA.port_src = "KOSONG";
                        st.nextToken();
                        String temp_dst = st.nextToken();
                        StringTokenizer s3 = new StringTokenizer(temp_dst,":");
                        AA.ip_dst = s3.nextToken();
                        if (s3.hasMoreTokens()) AA.port_dst = s3.nextToken();
                        else AA.port_dst = "KOSONG";
                        
                        //System.out.println(AA);
                        
                        ArrayList<SnortAlert> l = AnswerList.get(AA.waktu);
                        for (int i=0; i<l.size(); i++){
                            String hasil = AA.banding(l.get(i));
                            if (!hasil.equals("SALAH")) {
                                out.println(AA.toString() + " "+hasil);
                                //System.out.println("Verdict: "+hasil);
                                if (hasil.equals("-")) countFalse++;
                                else countTrue++;
                                break;
                            }
                        }
                    }
                }
            }
            out.println("True Positive SNORT: "+countTrue);
            out.println("False Negative SNORT: "+(jmlPositif-countTrue));
            out.println("False Positive SNORT: "+countFalse);
            out.println("True Negative SNORT: "+(jmlNegatif-countFalse));
            out.format("Precision: %f\n", (countTrue / (countTrue + countFalse)));
            out.format("Recall: %f\n", (countTrue / jmlPositif));
            out.format("Accuracy: %f\n", ((countTrue + jmlNegatif - countFalse) / (jmlNegatif + jmlPositif)));
            
            out.close();
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
