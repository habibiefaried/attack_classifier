/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converter;

import java.util.Date;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import NetworkPacket4Converter.NetworkObject;
import NetworkPacket4Converter.networkElmt;
import Settings.GlobalSettings;
import org.jnetpcap.Pcap;  
import org.jnetpcap.PcapHeader;  
import org.jnetpcap.nio.JBuffer;  
import org.jnetpcap.nio.JMemory;  
import org.jnetpcap.packet.JRegistry;  
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;  
import org.jnetpcap.packet.format.FormatUtils;  
import org.jnetpcap.protocol.lan.Ethernet;  
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

/**
 * menggabungkan dan convert tcpdump dan tcplist
 *
 * @author LightMind
 */
public class converterapp {
    private static final String active_dir = GlobalSettings.active_dir;
    public static HashMap<Integer, networkElmt> dataTest = new HashMap<>();
        
    public static void readTCPList(){
        try {
            final String FILE_NAME;
            FILE_NAME = active_dir + "tcpout.list";
            FileReader file_to_read;
            file_to_read = new FileReader(FILE_NAME);
            BufferedReader bf = new BufferedReader(file_to_read);
            String aLine;
            String lompat;
            int i = 1;
            
            while ((aLine = bf.readLine()) != null){
                StringTokenizer defaultTokenizer = new StringTokenizer(aLine);  
                networkElmt nE = new networkElmt(); //hanya sebagai temporary object
                nE.FrameNumber = Integer.parseInt(defaultTokenizer.nextToken());
                
                
                String dateInString = defaultTokenizer.nextToken() + " "+ defaultTokenizer.nextToken();                
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date date = formatter.parse(dateInString);
                
                nE.timestamp = date.toString();
                nE.time_packet = date.getTime() / 1000;
                
                String durasi = defaultTokenizer.nextToken();
                String service = defaultTokenizer.nextToken();
                
                String port_src = defaultTokenizer.nextToken();
                if (!"-".equals(port_src)) nE.port_src = Integer.parseInt(port_src);
                
                String port_dst = defaultTokenizer.nextToken();
                if (!"-".equals(port_dst)) nE.port_dst = Integer.parseInt(port_dst);
                
                nE.ip_src = GlobalSettings.normalizeIP(defaultTokenizer.nextToken());
                nE.ip_dst = GlobalSettings.normalizeIP(defaultTokenizer.nextToken());
                
                lompat = defaultTokenizer.nextToken();
                nE.label_attack = defaultTokenizer.nextToken();
                                
                NetworkObject.insertNetworkObject(nE.time_packet,nE); //taro di network object
                i++;
            }
            
            System.out.println("Seluruh koneksi terbaca: "+i);
            bf.close();
        } catch (Exception e){e.printStackTrace();}
    }
    
    public static void readPCAPandConvert() throws IOException{
        
        final String FILE_NAME = active_dir + "outside.tcpdump";  
        StringBuilder errbuf = new StringBuilder(); // For any error msgs  
        
        Pcap pcap = Pcap.openOffline(FILE_NAME, errbuf);  
        if (pcap == null) {  
            System.err.printf("Error while opening file for capture: "  + errbuf.toString());  
            return;  
        } 
        
        PcapHeader hdr = new PcapHeader(JMemory.POINTER);  
        JBuffer buf = new JBuffer(JMemory.POINTER);  
        
        int id = JRegistry.mapDLTToId(pcap.datalink());  
        //int MAX_SCAN = 100000; //maximum pcap read scan
        
        int iterasi = 0;
        
        while (pcap.nextEx(hdr, buf) == Pcap.NEXT_EX_OK) { 
            iterasi++;
            //if (iterasi > MAX_SCAN) break;
            
            PcapPacket packet = new PcapPacket(hdr, buf);  
            packet.scan(id); 
            
            Payload payload = new Payload();
            Ip4 ip = new Ip4();  
            Ethernet eth = new Ethernet();  
            Tcp tcp = new Tcp();
            Udp udp = new Udp();
            Icmp icmp = new Icmp();
           
            int port_src = -1;
            int port_dst = -1;
            int payload_size = 0;
            
            String jenis_paket = "";
            String tcpFlags = "";
            Set<Tcp.Flag> cntrlFlags = null;
            
            if (packet.hasHeader(ip)){
                //dapet payload setelah IP
                payload_size = packet.getHeader(ip).getOffset() + packet.getHeader(ip).size();
            }
            
            if (packet.hasHeader(tcp)){
                //layer 4 if TCP
                port_src = packet.getHeader(tcp).source();
                port_dst = packet.getHeader(tcp).destination();
                jenis_paket = "TCP";
                /*
                for (Tcp.Flag cntrlFlag : packet.getHeader(tcp).flagsEnum()) {
                    tcpFlags = tcpFlags + cntrlFlag.toString() + " ";
                } */
                cntrlFlags =  packet.getHeader(tcp).flagsEnum();
            }
            
            if (packet.hasHeader(udp)){
                port_src = packet.getHeader(udp).source();
                port_dst = packet.getHeader(udp).destination();
                jenis_paket = "UDP";
            }
            
            if (packet.hasHeader(icmp)){
                jenis_paket = "ICMP";
                port_src = 0;
                port_dst = 0;
            }
                      
            long tp = (packet.getCaptureHeader().timestampInMillis() - (11 * 3600 * 1000)) / 1000; //time drift 7 jam
            //System.out.println("Timepacket: "+tp);
            
            networkElmt NE = getTrueElmt(tp,port_src, port_dst, packet.getCaptureHeader().timestampInMillis());
            
            if (NE != null) {              
                NE.tipe_paket = jenis_paket;
                //out.println(NE.FrameNumber+delimiters+NE.time_packet+delimiters
                //+verdict+delimiters+NE.label_attack+delimiters+NE.port_src+delimiters+NE.port_dst+delimiters+NE.ip_src+delimiters+NE.ip_dst+delimiters+jenis_paket+delimiters+tcpFlags);
                if (dataTest.get(NE.FrameNumber) == null) {
                    if (cntrlFlags != null) {
                        for (Tcp.Flag cntrlFlag : cntrlFlags) NE.tcp_flags.add(cntrlFlag.toString());
                    }
                    
                    dataTest.put(NE.FrameNumber, NE);
                    NE.jumlahPaket = 1;
                    NE.payload_length = payload_size;
                }
                else {
                    NE.jumlahPaket++;
                    NE.payload_length += payload_size; //increment
                    if (cntrlFlags != null) {
                        for (Tcp.Flag cntrlFlag : cntrlFlags)
                            dataTest.get(NE.FrameNumber).tcp_flags.add(cntrlFlag.toString());
                    }
                }
            }
        }
        System.out.println("Iterasi: "+iterasi);
    }
    
    private static void WriteDatasetNN() throws IOException{
        PrintWriter out = new PrintWriter(new FileWriter(GlobalSettings.learn_dir+GlobalSettings.week+"-"+GlobalSettings.day+"-NN.learn")); 
        System.out.println("Akan ditulis "+dataTest.size()+" data");
        Set set = dataTest.entrySet();
        Iterator i = set.iterator();
        while(i.hasNext()) {
           Map.Entry me = (Map.Entry)i.next();
           networkElmt NE = (networkElmt) me.getValue();
           String delimiters = " ";
           int verdict = 0;
           if (!"-".equals(NE.label_attack)) verdict = 1;
           
           StringBuilder sb = new StringBuilder();
           for (String s : NE.tcp_flags) {
                sb.append(s);
                sb.append(" ");
           }
           
           out.println( 
                        NE.FrameNumber+delimiters+
                        NE.timestamp+delimiters+
                        NE.time_packet+delimiters+
                        verdict+delimiters+
                        NE.label_attack+delimiters+
                        NE.port_src+delimiters+
                        NE.port_dst+delimiters+
                        NE.ip_src+delimiters+
                        NE.ip_dst+delimiters+
                        NE.tipe_paket+delimiters+
                        NE.jumlahPaket+delimiters+
                        NE.payload_length+delimiters+
                        sb.toString());
           
           //MFL.insertDataTestFuzzySend(NE.ip_src, NE.timestamp, NE.time_packet, NE.ip_dst, NE.label_attack, NE.FrameNumber); //sambil nulis, sambil ngitung
           //MFL.insertDataTestFuzzyRecv(NE.ip_dst, NE.timestamp, NE.time_packet, NE.ip_src, NE.label_attack, NE.FrameNumber); //sambil nulis, sambil ngitung
        }
        
        out.close();
    }
    
    private static networkElmt getTrueElmt(long timepacket, int port_src, int port_dst, long tcptgl){
        //inspect 897368715 (salah waktu)
        //Secondary identifier adalah port_src yang digunakan. Apabila sesuai maka benar
        List<networkElmt> N = NetworkObject.getNL().get(timepacket);
        if ((N == null) || (port_src == -1)) return null;
        else {
            //System.out.println("[KETEMU] "+timepacket+". Size: "+N.size()+". TGL: "+new Date(tcptgl).toString());
            for (networkElmt N1 : N) {
                //System.out.println("N1: "+N1.port_src+". P: "+port_src+". Tgl: "+N1.timestamp);
                if ((N1.port_src == port_src) && (N1.port_dst == port_dst)) {
                    return N1;
                }
            }
            return null;
        }
    }
    
    public static void main(String args[]) throws IOException{
        new GlobalSettings();
        readTCPList(); //~25 detik
        System.out.println("Terekam: "+NetworkObject.getNL().size());
        System.out.println("Konversi Data...");
        readPCAPandConvert();
        System.out.println("Menulis NN fuzzy learn...");
        WriteDatasetNN();
        /*
        System.out.println("Menulis data fuzzy learn send");
        MFL.writeDataTestFuzzy("send");
        System.out.println("Menulis data fuzzy learn recv");
        MFL.writeDataTestFuzzy("recv"); */
    }
}
