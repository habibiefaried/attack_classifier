/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkPacket4Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author LightMind
 */

public class NetworkObject {
    private static HashMap<Long, List<networkElmt>> nE = new HashMap<>();
    public static int bentrok = 0;
    public static int terbaca = 0;
    
    public static void insertNetworkObject(long timepacket, networkElmt _nE){
        //System.out.println("Time: "+timepacket);
        List<networkElmt> al = nE.get(timepacket);
        if (al == null){
            nE.put(timepacket,new ArrayList<networkElmt>());
        } else bentrok++;
        nE.get(timepacket).add(_nE);
        terbaca++;
    }
    
    public static HashMap<Long, List<networkElmt>> getNL(){return nE;}
}
