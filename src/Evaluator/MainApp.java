/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import Settings.GlobalSettings;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;


/**
 *
 * @author LightMind
 */
public class MainApp {
    private static ArrayList<NNElmt> NearestNeighbourData = new ArrayList<>();
    private static PrintWriter out; 
    
    private static void readDataSet() throws FileNotFoundException, IOException{
        String FILE_NAME;
        
        if (GlobalSettings.terbalik)
            FILE_NAME = GlobalSettings.dataset;
        else {
            if (GlobalSettings.useRealDataset) FILE_NAME = GlobalSettings.learn_dir+"dataset-full.learn";
            else FILE_NAME = GlobalSettings.learn_dir+GlobalSettings.week+"-"+GlobalSettings.day+"-NN.learn";
        }
        
        
        FileReader file_to_read;
        file_to_read = new FileReader(FILE_NAME);
        BufferedReader bf = new BufferedReader(file_to_read);
        String aLine;
        String lompat;
        
        while ((aLine = bf.readLine()) != null){
            NNElmt NN = new NNElmt();
            StringTokenizer defaultTokenizer = new StringTokenizer(aLine);  
            NN.FrameNumber = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            NN.timepacket = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            String temp_verdict = defaultTokenizer.nextToken();
            NN.port_src = Integer.parseInt(defaultTokenizer.nextToken());
            NN.port_dst = Integer.parseInt(defaultTokenizer.nextToken());
            NN.ip_src = defaultTokenizer.nextToken();
            NN.ip_dst = defaultTokenizer.nextToken();
            NN.jenis_paket = defaultTokenizer.nextToken();
            NN.jumlah_paket = Integer.parseInt(defaultTokenizer.nextToken());
            NN.payload_size = Integer.parseInt(defaultTokenizer.nextToken());
            while (defaultTokenizer.hasMoreTokens()){
                String str_flag = defaultTokenizer.nextToken();
                if ("FIN".equals(str_flag)) NN.jumlah_FIN++;
                if ("SYN".equals(str_flag)) NN.jumlah_SYN++;
                if ("RST".equals(str_flag)) NN.jumlah_RST++;
                NN.tcpFlags.add(str_flag);
            }
            
            for (String retval: temp_verdict.split(",")){ //jika multilabel
                NN.verdict = retval;
                NearestNeighbourData.add(NN);
            }
        }
        System.out.println("Terbaca: "+FILE_NAME);
    }
    
    private static void readAndClassifyListTest() throws FileNotFoundException, IOException{
        FileReader file_to_read;
        String FILE_NAME="";
        if (!GlobalSettings.terbalik)
            FILE_NAME = GlobalSettings.work_file;
        else 
            FILE_NAME = GlobalSettings.datatest;
        
        file_to_read = new FileReader(FILE_NAME);
        
        System.out.println("Sedang dibaca: "+FILE_NAME);
        BufferedReader bf = new BufferedReader(file_to_read);
        String aLine;
        String lompat;
        
        double FalseNegative = 0;
        double FalsePositive = 0;
        double TrueNegative = 0;
        double TruePositive = 0;
        double PositiveInstance = 0;
        double NegativeInstance = 0;
        int jumlahKoneksi = 0;
        
        while ((aLine = bf.readLine()) != null){
            ArrayList<NearestNeighbourResult> nnList = new ArrayList<>();
            NNElmt NN = new NNElmt();
            StringTokenizer defaultTokenizer = new StringTokenizer(aLine);  
            NN.FrameNumber = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            NN.timepacket = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            NN.verdict = defaultTokenizer.nextToken();
            NN.port_src = Integer.parseInt(defaultTokenizer.nextToken());
            NN.port_dst = Integer.parseInt(defaultTokenizer.nextToken());
            NN.ip_src = defaultTokenizer.nextToken();
            NN.ip_dst = defaultTokenizer.nextToken();
            NN.jenis_paket = defaultTokenizer.nextToken();
            NN.jumlah_paket = Integer.parseInt(defaultTokenizer.nextToken());
            NN.payload_size = Integer.parseInt(defaultTokenizer.nextToken());
            while (defaultTokenizer.hasMoreTokens()){
                String str_flag = defaultTokenizer.nextToken();
                if ("FIN".equals(str_flag)) NN.jumlah_FIN++;
                if ("SYN".equals(str_flag)) NN.jumlah_SYN++;
                if ("RST".equals(str_flag)) NN.jumlah_RST++;
                NN.tcpFlags.add(str_flag);
            }
            //System.out.println("Frame: "+NN.FrameNumber);
            nnList = getAllScores(NN);
            //System.out.println("Frame selesai");
            String verdict = determineVerdict(nnList,NN.FrameNumber);
            //System.out.println("Verdict: "+verdict);
            
            if (!"-".equals(verdict)) { //hasil positive
                if (!"-".equals(NN.verdict)) { //jawaban positive
                    TruePositive++;
                    PositiveInstance++;
                }
                else { //jawaban negative
                    FalsePositive++;
                    NegativeInstance++;
                }
            } else { //hasil negative (-)
                if ("-".equals(NN.verdict)) { //jawaban negative
                    TrueNegative++;
                    NegativeInstance++;
                }
                else { //jawaban positive
                    FalseNegative++;
                    PositiveInstance++;
                }
            }
            out.println(verdict+" "+NN.verdict+" "+NN.ip_src+" "+NN.ip_dst+" "+NN.port_src+" "+NN.port_dst);
            out.println("");
            out.println("");
            jumlahKoneksi++;
        }
        
        out.println("KESIMPULAN");
        out.println("True Positive: "+TruePositive);
        out.println("False Positive: "+FalsePositive);
        out.println("True Negative: "+TrueNegative);
        out.println("False Negative: "+FalseNegative);
        out.println("Jumlah Koneksi: "+jumlahKoneksi);
        out.format("Precision: %f\n", (TruePositive / (TruePositive + FalsePositive)));
        out.format("Recall: %f\n", (TruePositive / (TruePositive + FalseNegative)));
        out.format("Accuracy: %f\n", ((TruePositive + TrueNegative) / (PositiveInstance + NegativeInstance)));
        System.out.println("Klasifikasi dengan k-nn selesai");
    }
    
    public static String determineVerdict(ArrayList<NearestNeighbourResult> in, int FrameNumber){
        //System.out.println("Fn: "+FrameNumber);
        HashMap<String, Integer> ranking = new HashMap<>();
        
        out.println("Frame Number: "+FrameNumber);
        int count = 0;
        int batas = in.size();
        int ukur = batas;
        
        int countAttack = 0;
        int countNormal = 0;
        
        if (GlobalSettings.MaximumNeighbour < batas) batas = GlobalSettings.MaximumNeighbour;
        
        int minValue = 0; //pasti value yang selalu terkecil
        while (count < batas) {   
            NearestNeighbourResult NR = in.get(count);
            //out.println(NR);
            String v = NR.verdict;
            minValue = NR.skor;
            if (ranking.get(v) == null){
                ranking.put(v, 1);
            } else {
                int value = ranking.get(v);
                value++;
                ranking.put(v, value);
            }
            count++;
            //if (in.get(count).verdict == "-") countNormal++;
            //else countAttack;
        }
        
        String hasil_attack="-";
        int jumlah_vote=0;

        //out.println("PERHITUNGAN ATTACK");
        //out.println("===================================================================");
        Set set = ranking.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            int value = (Integer) me.getValue();
            if (value > jumlah_vote){
                jumlah_vote = value;
                hasil_attack = (String) me.getKey();
            }
            //out.println(me.getKey()+" "+value);
        }
        return hasil_attack;
    }
    
    
    private static ArrayList<NearestNeighbourResult> getAllScores(NNElmt nn){
        ArrayList<NearestNeighbourResult> res = new ArrayList<>();
        for (NNElmt ne : NearestNeighbourData){
            NearestNeighbourResult NNR = new NearestNeighbourResult();
            NNR.ip_dst = ne.ip_dst;
            NNR.ip_src = ne.ip_src;
            NNR.port_src = ne.port_src;
            NNR.port_dst = ne.port_dst;
            NNR.verdict = ne.verdict;
            NNR.skor = ne.compareNNElmt(nn); //kan ada skor
            res.add(NNR);
            
        }
        Collections.sort(res, NearestNeighbourResult.skorComp); //masalah disini
        return res;
    }
    
    /*
    private static LinkedList<NearestNeighbourResult> getAllScores(NNElmt nn){
        LinkedList<NearestNeighbourResult> res = new LinkedList<>();
        //System.out.println("AAA");
        for (NNElmt ne : NearestNeighbourData){
            //System.out.println("B");
            NearestNeighbourResult NNR = new NearestNeighbourResult();
            NNR.ip_dst = ne.ip_dst;
            NNR.ip_src = ne.ip_src;
            NNR.port_src = ne.port_src;
            NNR.port_dst = ne.port_dst;
            NNR.verdict = ne.verdict;
            NNR.skor = ne.compareNNElmt(nn); //kan ada skor
            
            if (res.size() == 0) res.add(NNR);
            else {
                int i = 0;
                while ((NNR.skor < res.get(i).skor) && (i != (res.size()-1))) i++;
                
                if (i == (res.size() - 1)) res.addLast(NNR);
                else res.add(i, NNR);
            }
        }
        return res;
    }
    */
    
    public static void main(String args[]) throws IOException{   
        Runtime runtime = Runtime.getRuntime();
        
        if (!GlobalSettings.terbalik)
            out = new PrintWriter(new FileWriter(GlobalSettings.hasil_file));
        else 
            out = new PrintWriter(new FileWriter(GlobalSettings.datatest+".hasil"));

        readDataSet();
        //Sorting dulu
        System.out.println("Sorting");
        Collections.sort(NearestNeighbourData, NNElmt.attackComp); //masalah disini
        
        readAndClassifyListTest();
        out.close(); //batas bintang
        
        

        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        sb.append("Free Memory: ").append(format.format(freeMemory / 1024)).append("<br/>");
        sb.append("Allocated memory: ").append(format.format(allocatedMemory / 1024)).append("<br/>");
        sb.append("Max Memory: ").append(format.format(maxMemory / 1024)).append("<br/>");
        sb.append("Total Free Memory: ").append(format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)).append("<br/>");
        System.out.println("Telah selesai");
        System.out.println(sb.toString());
    }
}
