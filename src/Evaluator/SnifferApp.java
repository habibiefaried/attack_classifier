/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import Settings.GlobalSettings;
import Settings.ShutdownHook;
import Settings.StopThread;
import java.io.BufferedReader;
import java.io.File;  
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;  
import java.util.ArrayList;  
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;  
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
  
import org.jnetpcap.Pcap;  
import org.jnetpcap.PcapDumper;  
import org.jnetpcap.PcapHandler;  
import org.jnetpcap.PcapIf; 
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

/**
 *
 * @author LightMind
 */
public class SnifferApp {    
    private static ArrayList<NNElmt> NearestNeighbourData = new ArrayList<>();
    private static NNElmt LastSession = new NNElmt();
    public static int JmlPaket = 0;
    public static double JmlDelay = 0;
    public static double JmlDelayKuadrat = 0;
    //private static int MaxRecvPacket = 100;
    public static double MaxDelay = 0;
    public static double MinDelay = 999999;
    
    private static void resetLastSession(){
        LastSession.timepacket = 0;
        LastSession.ip_src = "";
        LastSession.ip_dst = "";
        LastSession.port_dst = 0;
        LastSession.port_src = 0;
        LastSession.jenis_paket = "";
        LastSession.jumlah_paket = 0;
        LastSession.payload_size = 0;
        LastSession.tcpFlags = new ArrayList<String>();
    }
    
    private static void setLastSession(long tp, String ip_src, String ip_dst, int port_src, int port_dst, String jenis_paket, int payload_size, String tcpFlags){
        LastSession.timepacket = tp;
        LastSession.ip_src = ip_src;
        LastSession.ip_dst = ip_dst;
        LastSession.port_src = port_src;
        LastSession.port_dst = port_dst;
        LastSession.jenis_paket = jenis_paket;
        LastSession.jumlah_paket = 1;
        LastSession.payload_size = payload_size;
        LastSession.tcpFlags.add(tcpFlags);
    }
    
    private static void addLastSession(int payload_size, String tcpFlags){
        LastSession.payload_size += payload_size;
        LastSession.tcpFlags.add(tcpFlags);
        LastSession.jumlah_paket++;
    }
    
    private static void readDataSet() throws FileNotFoundException, IOException{
        String FILE_NAME;
        FILE_NAME = GlobalSettings.learn_dir+"dataset-full.learn";
       
        
        FileReader file_to_read;
        file_to_read = new FileReader(FILE_NAME);
        BufferedReader bf = new BufferedReader(file_to_read);
        String aLine;
        String lompat;
        
        while ((aLine = bf.readLine()) != null){
            NNElmt NN = new NNElmt();
            StringTokenizer defaultTokenizer = new StringTokenizer(aLine);  
            NN.FrameNumber = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            NN.timepacket = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            String temp_verdict = defaultTokenizer.nextToken();
            NN.port_src = Integer.parseInt(defaultTokenizer.nextToken());
            NN.port_dst = Integer.parseInt(defaultTokenizer.nextToken());
            NN.ip_src = defaultTokenizer.nextToken();
            NN.ip_dst = defaultTokenizer.nextToken();
            NN.jenis_paket = defaultTokenizer.nextToken();
            NN.jumlah_paket = Integer.parseInt(defaultTokenizer.nextToken());
            NN.payload_size = Integer.parseInt(defaultTokenizer.nextToken());
            while (defaultTokenizer.hasMoreTokens()){
                String str_flag = defaultTokenizer.nextToken();
                if ("FIN".equals(str_flag)) NN.jumlah_FIN++;
                if ("SYN".equals(str_flag)) NN.jumlah_SYN++;
                if ("RST".equals(str_flag)) NN.jumlah_RST++;
                NN.tcpFlags.add(str_flag);
            }
            
            for (String retval: temp_verdict.split(",")){ //jika multilabel
                NN.verdict = retval;
                NearestNeighbourData.add(NN);
            }
        }
        System.out.println("Terbaca: "+FILE_NAME);
    }
    
    public static String determineVerdict(ArrayList<NearestNeighbourResult> in){
        //System.out.println("Fn: "+FrameNumber);
        HashMap<String, Integer> ranking = new HashMap<>();
        
        int count = 0;
        int batas = in.size();
        
        int countAttack = 0;
        int countNormal = 0;
        
        if (GlobalSettings.MaximumNeighbour < in.size()) batas = GlobalSettings.MaximumNeighbour;
        
        while (count < batas) {   
            String v = in.get(count).verdict;
            if (ranking.get(v) == null){
                ranking.put(v, 1);
            } else {
                int value = ranking.get(v);
                value++;
                ranking.put(v, value);
            }
            count++;
            //if (in.get(count).verdict == "-") countNormal++;
            //else countAttack;
        }
        
        String hasil_attack="-";
        int jumlah_vote=0;

        //out.println("PERHITUNGAN ATTACK");
        //out.println("===================================================================");
        Set set = ranking.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            int value = (Integer) me.getValue();
            if (value > jumlah_vote){
                jumlah_vote = value;
                hasil_attack = (String) me.getKey();
            }
            //out.println(me.getKey()+" "+value);
        }
        return hasil_attack;
    }
    
    
    private static ArrayList<NearestNeighbourResult> getAllScores(){
        ArrayList<NearestNeighbourResult> res = new ArrayList<>();
        for (NNElmt ne : NearestNeighbourData){
            NearestNeighbourResult NNR = new NearestNeighbourResult();
            NNR.ip_dst = ne.ip_dst;
            NNR.ip_src = ne.ip_src;
            NNR.port_src = ne.port_src;
            NNR.port_dst = ne.port_dst;
            NNR.verdict = ne.verdict;
            NNR.skor = ne.compareNNElmt(LastSession); //kan ada skor
            res.add(NNR);
        }
        Collections.sort(res, NearestNeighbourResult.skorComp); 
        return res;
    }
    
    public static void main(String args[]) throws IOException{
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());
        PrintStream out = new PrintStream(new FileOutputStream(GlobalSettings.learn_dir+"logIDS.txt"));
        StopThread st = new StopThread();
        st.start();
        System.setOut(out);

        long tStart = System.currentTimeMillis();
        
        readDataSet();
        System.out.println("Sorting");
        Collections.sort(NearestNeighbourData, NNElmt.attackComp); //masalah disini
        
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        System.out.println("Dataset selesai dibaca: "+NearestNeighbourData.size());
        System.out.println("Waktu untuk membaca dataset: "+(tDelta / 1000.0)+" detik");
        //System.exit(1);
        
        
        List<PcapIf> alldevs = new ArrayList<PcapIf>(); // Will be filled with NICs  
        StringBuilder errbuf = new StringBuilder();     // For any error msgs  
        
        /*************************************************************************** 
        * First get a list of devices on this system 
        **************************************************************************/  
       int r = Pcap.findAllDevs(alldevs, errbuf);  
       if (r == Pcap.NOT_OK || alldevs.isEmpty()) {  
         System.err.printf("Can't read list of devices, error is %s\n",   
           errbuf.toString());  
         return;  
       }         
       System.out.println("Network devices found:");  
        int i = 0;  
        for (PcapIf device : alldevs) {  
            String description =  
                (device.getDescription() != null) ? device.getDescription()  
                    : "No description available";  
            System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);  
        }  
  
        PcapIf device = alldevs.get(1); // We know we have atleast 1 device, listen ke VM
        System.out.printf("\nChoosing '%s' on your behalf:\n",  (device.getDescription() != null) ? device.getDescription() : device.getName());  
        
        /*************************************************************************** 
        * Second we open up the selected device 
        **************************************************************************/  
       int snaplen = 64 * 1024;           // Capture all packets, no trucation  
       int flags = Pcap.MODE_PROMISCUOUS; // capture all packets  
       int timeout = 10 * 1000;           // 10 seconds in millis  
       Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, errbuf);  
       if (pcap == null) {  
         System.err.printf("Error while opening device for capture: %s\n",   
           errbuf.toString());  
         return;  
       }
       
        String ofile = "C:\\logTA\\tmp-capture-file.pcap";  
        PcapDumper dumper = pcap.dumpOpen(ofile); // output file  
  
        PcapPacketHandler<String> jpacketHandler;  
        jpacketHandler = (PcapPacket packet, String user) -> {
            pcapdump(packet);
        };  
        
        /*************************************************************************** 
         * Fourth we enter the loop and tell it to capture 10 packets. The loop 
         * method does a mapping of pcap.datalink() DLT value to JProtocol ID, which 
         * is needed by JScanner. The scanner scans the packet buffer and decodes 
         * the headers. The mapping is done automatically, although a variation on 
         * the loop method exists that allows the programmer to sepecify exactly 
         * which protocol ID to use as the data link type for this pcap interface. 
         **************************************************************************/  
        pcap.loop(pcap.LOOP_INFINITE, jpacketHandler, "jNetPcap rocks!");  
  
        /*************************************************************************** 
         * Last thing to do is close the pcap handle 
         **************************************************************************/  
        pcap.close();
        
    }
    
    private static void pcapdump(PcapPacket packet) {
       // System.out.println("A");
        Payload payload = new Payload();
        Ip4 ip = new Ip4();  
        Ethernet eth = new Ethernet();  
        Tcp tcp = new Tcp();
        Udp udp = new Udp();
        Icmp icmp = new Icmp();
        
        int port_src = -1;
        int port_dst = -1;
        int payload_size = 0;
        Set<Tcp.Flag> cntrlFlags = null;
        String tcpFlags = "";
        String ip_src = "-";
        String ip_dst = "-";
        String jenis_paket = "-";
        String mac_src = "-";
        String mac_dst = "-";
        
        long tp = packet.getCaptureHeader().timestampInMillis();
        String tanggal = new Date(tp).toString();
        
        if (packet.hasHeader(eth)){
            mac_src = FormatUtils.mac(eth.source()); 
            mac_dst = FormatUtils.mac(eth.destination());
        }
        
        if (packet.hasHeader(ip)){
                //dapet payload setelah IP
                //System.out.println("IP");
                ip_src = FormatUtils.ip(ip.source());
                ip_dst = FormatUtils.ip(ip.destination());
                payload_size = ip.getOffset() + ip.size();
        }
        
        if (packet.hasHeader(tcp)){
                //layer 4 if TCP
                //System.out.println("INI TCP");
                port_src = packet.getHeader(tcp).source();
                port_dst = packet.getHeader(tcp).destination();
                jenis_paket = "TCP";
                for (Tcp.Flag cntrlFlag : packet.getHeader(tcp).flagsEnum()) {
                    tcpFlags = tcpFlags + cntrlFlag.toString() + " ";
                }
                if (tcpFlags.length() != 0) tcpFlags = tcpFlags.substring(0,tcpFlags.length()-1);
        }
        
        if (packet.hasHeader(udp)){
                port_src = packet.getHeader(udp).source();
                port_dst = packet.getHeader(udp).destination();
                jenis_paket = "UDP";
        }
        
        if (packet.hasHeader(icmp)){
                jenis_paket = "ICMP";
                port_src = 0;
                port_dst = 0;
        }
        
        
        if ((!ip_src.equals("-")) && (!ip_dst.equals("-")) && (!jenis_paket.equals("-"))) {
            System.out.println(tanggal+" "+tp+" "+mac_src+" "+mac_dst+" "+ip_src+" "+ip_dst+" "+port_src+" "+port_dst+" "+jenis_paket+" "+payload_size+" "+tcpFlags);
            
            if (LastSession.timepacket != 0){
                if (((tp == LastSession.timepacket) && (port_src == LastSession.port_src) && (port_dst == LastSession.port_dst)) || ((tp == LastSession.timepacket) && (port_src == LastSession.port_dst) && (port_dst == LastSession.port_src))) {
                    addLastSession(payload_size, tcpFlags); //tambah ke session yang ada
                } else { //kalau sudah beda
                    JmlPaket++;
                    System.out.print(LastSession);
                    //Klasifikasikan session tersebut
                    
                    long tStart = System.currentTimeMillis();
                    String verdict = determineVerdict(getAllScores());
                    long tEnd = System.currentTimeMillis();
                    long tDelta = tEnd - tStart;
                    
                    if (verdict.equals(("-"))) System.out.println(" : normal"); //tentukan verdict
                    else System.out.println(" : attack"); //tentukan verdict
                    System.out.println("Delay: "+tDelta);
                    
                    if (MaxDelay < tDelta) MaxDelay = tDelta;
                    if (MinDelay > tDelta) MinDelay = tDelta;
                    
                    JmlDelay = JmlDelay + tDelta;
                    JmlDelayKuadrat = JmlDelayKuadrat + Math.pow(tDelta, 2);
                    
                    //if (JmlPaket > MaxRecvPacket) System.exit(-1);
                    
                    resetLastSession(); //reset
                    setLastSession(tp,ip_src, ip_dst, port_src, port_dst, jenis_paket, payload_size, tcpFlags); //reset
                }
            } else setLastSession(tp,ip_src, ip_dst, port_src, port_dst, jenis_paket, payload_size, tcpFlags); //set baru
        }
        
    }
}
